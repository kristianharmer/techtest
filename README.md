
## Laravel Technical Test (Laravel 6+)

- Run 'composer update'
- Rename .env.example to .env and add your local database credentials
- Run 'php artisan migrate'
- Optional seeder available if needed
