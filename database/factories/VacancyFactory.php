<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vacancy;
use Faker\Generator as Faker;

$factory->define(Vacancy::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'description' => $faker->sentence(50),
        'location' => $faker->city,
        'salary' => '£' . rand(20000, 30000) . ' - ' . rand(40000, 50000),
    ];
});
