<!-- Bootstrap message blocks -->
@if(Session::has('message'))
  <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('message') }}
  </div>
@endif
@if(Session::has('warning'))
  <div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('warning') }}
  </div>
@endif
@if(Session::has('danger') && Session::get('danger') != '')
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><strong>Warning:</strong> Please notify system admin of the following message.</p>
    <code>{{ Session::get('danger') }}</code>
  </div>
@endif
@if(Session::has('info'))
  <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {{ Session::get('info') }}
  </div>
@endif