@extends('layouts.admin')

@section('content')

    <div class="row">

      <aside class="sidebar">
        <h2>Filter Vacancies</h2>
          <form action="{{ route('vacancies.index') }}" method="GET">
            <div class="form-group">
              <label for="keywords" >Keyword</label>
                <input name="keywords" type="text" class="form-control" id="keywords" value="{{ $keywords }}" placeholder="i.e. Job Title or Technology">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Filter Results</button>
          </form>
      </aside>

      <main>

        @include('partials/messages')

        <div class="container add-new">
          <div class="row">
            <div class="col-lg-9 col-sm-8 vacancy-count">Available Vancies ({{ $vacancies->total() }})</div>
            <div class="col-lg-3 col-sm-4">
              <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                Add new vacancy
              </button>
            </div>
          </div>
        </div>

            {{-- Collapsable form - start --}}
            <div class="collapse" id="collapseExample">
              <div class="card card-body">
                
                <form action="{{ route('vacancies.index') }}" method="POST" class="add-vacancy" novalidate>

                  @method('POST')

                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group row">
                    <label for="title" class="col-md-3 col-form-label">Title</label>
                    <div class="col-md-9">
                      <input type="text" name="title" class="form-control" id="title" placeholder="Title" maxlength="50" required>
                      <div class="invalid-feedback">
                        Please provide a Title for this vacancy
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="description" class="col-md-3 col-form-label">Description</label>
                    <div class="col-md-9">
                      <textarea name="description" class="form-control" id="description" rows="5" required></textarea>
                      <div class="invalid-feedback">
                        Please provide a Description for this vacancy
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="location" class="col-md-3 col-form-label">Location</label>
                    <div class="col-md-9">
                      <input name="location" type="text" class="form-control" id="location" maxlength="20" required>
                      <div class="invalid-feedback">
                        Please provide a Location for this vacancy
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="salary" class="col-md-3 col-form-label">Salary</label>
                    <div class="col-md-9">
                      <input name="salary" type="text" class="form-control" id="salary" maxlength="20" required>
                      <div class="invalid-feedback">
                        Please provide a Salary or Salary Range for this vacancy
                      </div>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Submit</button>

                </form>
              </div>
            </div>
            {{-- Collapsable form - end --}}
  
        @if($vacancies->count() > 0)

          @foreach($vacancies as $vacancy)

            <article>
              <div class="body">
                <h1>{{ $vacancy->title }}</h1>
                <ul class="vacancy-meta">
                  <li class="item"><i class="fas fa-map-marker-alt"></i>{{ $vacancy->location }}</li>
                  <li class="item"><i class="fas fa-pound-sign"></i>{{ $vacancy->salary }}</li>
                </ul>
                
                <p class="card-text">{{ $vacancy->description }}</p>

                <a href="#" class="btn btn-sm btn-default">More details</a>
                <a href="#" class="btn btn-sm btn-primary">Apply</a>
              </div>
            </article>

          @endforeach

          {!! $vacancies->links() !!}

        @else

          <p class="no-results">No results found!</p>

        @endif

      </main>

    </div>

@endsection